
#include <iostream>
#include <string>
using namespace std;


class Poczatek
{
protected:
Poczatek(string);
public:
string tekst;
~Poczatek();
void opis();
void wczytaj();
virtual void szyfruj()=0;
virtual void deszyfruj()=0;
virtual void odczyt()=0;
virtual void zapis()=0;
	
};
class Rot13:public Poczatek
{

	string roto;
	public:
	Rot13(string,string );
	~Rot13();
	void opis();
	void szyfruj();
	void deszyfruj();
	void zapis();
	void odczyt();
	
	
};
class Cezar:public Poczatek
{
	string czarek;
	public:
	
	Cezar(string,string );
	~Cezar();
	void opis();
	void szyfruj();
	void deszyfruj();
	void odczyt();
	void zapis();

};
