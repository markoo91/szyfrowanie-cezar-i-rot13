#include <iostream>
#include <cstdlib>
#include <conio.h>
#include "szyfrowanie.h"
using namespace std;

int main()
{
	Rot13 r1("Galia","omni");
	Cezar c1("dividas","partes");
	Rot13 *wsk;
	wsk = & r1;
	Cezar *drugi;
	drugi= &c1;
	char c,wybor;
	do
	{

	cout<<endl;
	cout<<endl<<endl;
	cout<<"Witaj w szyfrowaniu i deszyfrowaniu wiadomosci"<<endl;
	cout<<"przy pomocy Rot13 i Szyfru Cezara "<<endl;
	cout<<"||Szyfrowanie Cezar   (c)       ||"<<endl;
	cout<<"||Deszyfrowanie Cezar (d)	||"<<endl;
	cout<<"||Szyfrowanie Rot13   (r) 	||"<<endl;
	cout<<"||Deszyfrowanie Rot13 (p)	||"<<endl;
	cout<<"||Wyjscie             (q) 	||"<<endl;
	wybor=getch();
	while(wybor!='c'&&wybor!='d'&&wybor!='r'&&wybor!='p'&&wybor!='q')
	{
		cout <<"Podano zly znak  :("<<endl;
		wybor=getch();

	}
	switch(wybor)
	{
		case 'c':
		{
		drugi->opis();
		drugi->szyfruj();
		drugi->zapis();
		break;		
		}
		case 'd':
		{
		drugi->odczyt();
		drugi->deszyfruj();
		break;
		}
		case 'r':
		{
		wsk->opis();
		wsk->szyfruj();	
		wsk->zapis();
		break;
		}
		case 'p':
		{
		wsk->odczyt();
		wsk->deszyfruj();	
		break;
		}
		case 'q':
		{
			exit(0);
			break;
		}
	}
	cout<<"Wyjscie z programu klawisz (e) Wcisniecie innego klawiszu to powrot do menu "<<endl;
	c=getch();
	system("cls");
	}while(c!='e');
	cout<<"Dziala :)"<<endl;	
	return 0;
}
